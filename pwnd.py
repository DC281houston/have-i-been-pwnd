def getPass():
  import getpass as gp 
  import hashlib
  return hashlib.sha1(gp.getpass()).hexdigest() 

def checkHash(hs): 
  import requests
  h,s = hs[0:5],hs[5:]
  url = "https://api.pwnedpasswords.com/range/" + h
  r = requests.get(url)
  hashes = r.text.split('\r\n') 
  match = False
  for hsh in hashes:
    if s.upper() == hsh.split(':')[0].decode('UTF-8'):
      print('MATCH!  Used {}'.format(hsh.split(':')[1]))
      match = True
  if match == False:
    print('password not in the PWND database') 

if __name__ == '__main__':
  checkHash(getPass()) 
